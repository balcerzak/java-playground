package mustache;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.StringReader;
import java.util.HashMap;

import org.junit.Test;

public class MustacheTests {

    static class Student {
        String name, surname;
        boolean active;

        Student(String name, String surname, boolean active) {
            this.name = name;
            this.surname = surname;
            this.active = active;
        }
    }

    @Test
    public void mustacheSupportsStringReader() {

        // given:
        final String template = "Hello {{student.name}} {{student.surname}}";
        HashMap<String, Object> student = new HashMap<String, Object>();
        student.put("student", new Student("Lukasz", "Balcerzak", false));

        // when:
        final String output = MustacheTemplate.compileRawData(new StringReader(template), student);

        // then:
        assertThat(output, is("Hello Lukasz Balcerzak"));

    }

    @Test
    public void mustacheSupportsSections() {

        // given:
        final String template = "Hello {{student.name}} {{student.surname}}{{#student.active}} (active){{/student.active}}";
        HashMap<String, Object> student1 = new HashMap<String, Object>();
        student1.put("student", new Student("Lukasz", "Balcerzak", false));
        HashMap<String, Object> student2 = new HashMap<String, Object>();
        student2.put("student", new Student("Jan", "Kowalski", true));

        // when:
        final String studentOutput1 = MustacheTemplate.compileRawData(new StringReader(template), student1);
        final String studentOutput2 = MustacheTemplate.compileRawData(new StringReader(template), student2);

        // then:
        assertThat(studentOutput1, is("Hello Lukasz Balcerzak"));
        assertThat(studentOutput2, is("Hello Jan Kowalski (active)"));

    }

    @Test
    public void mustacheSupportsInvertedSections() {

        // TODO: propose given, when and then steps to prove that Mustache is able to support inverted sections

    }

}