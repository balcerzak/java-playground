package mustache;

import java.io.Reader;
import java.io.StringWriter;
import java.util.Map;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

public class MustacheTemplate {

    public static String compileRawData(final Reader template, final Map<String, Object> elements) {
        MustacheFactory factory = new DefaultMustacheFactory();
        StringWriter writer = new StringWriter();
        Mustache mustache = factory.compile(template, "myTemplate");
        mustache.execute(writer, elements);
        return writer.toString();
    }

    public static String compileFileData(String path, final Map<String, Object> elements) {
        MustacheFactory factory = new DefaultMustacheFactory();
        StringWriter writer = new StringWriter();
        Mustache mustache = factory.compile(path);
        mustache.execute(writer, elements);
        return writer.toString()
            .replaceAll("&quot;", "\"")
            .replaceAll(",\\s*}", "}")
            .replaceAll(",\\s*]", "]");
    }

}
