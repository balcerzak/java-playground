package cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {"classpath:features"},
    glue = {"cucumber/steps"},
    plugin = {"json:target/report/cucumber.json", "html:target/report/cucumber-html"},
    tags = {"not @Ignore"})
public class RunnerIT {
}
