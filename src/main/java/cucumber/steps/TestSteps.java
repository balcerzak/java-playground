package cucumber.steps;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import io.cucumber.datatable.DataTable;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import mustache.MustacheTemplate;

public class TestSteps {

    private static Response response;

    private void post(String payload) {
        response = given()
            .log().all()
            .contentType("application/json")
            .body(payload)
            .post("https://postman-echo.com/post");
    }

    @And("^user sends out following students list:$")
    public void userSendsOutFollowingStudentsList(final DataTable studentsList) {
        Map<String, Object> elements = new HashMap<>();
        elements.put("students", studentsList.asMaps(String.class, String.class));
        post(MustacheTemplate.compileFileData("templates/students.mustache", elements));
    }

    @And("^user sends out following engineers data:$")
    public void userSendsOutFollowingEngineersData(final DataTable engineerDetails) {
        post(MustacheTemplate.compileFileData("templates/engineer.mustache", engineerDetails.asMap(String.class, String.class)));
    }

    @Then("^response is according to \"(.*)\"$")
    public void responseIsAccordingTo(final String schema) {
        assertThat(response.getStatusCode(), is(200));
        assertThat(response.getBody().asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath(schema));
    }

}
