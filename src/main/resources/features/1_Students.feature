Feature: [1] Students

#    TODO:
#    1. Create new Mustache template in resources/templates
#    2. Design test step to send out POST request using already created template
#    3. Fill in test data and run Scenario 1

    Scenario: [1] Successfully post a list of students with at least 2 elements
        When user sends out following students list:
            | name   | surname   |
            | Lukasz | Balcerzak |
            | Jan    | Kowalski  |
        Then response is according to "schemas/students.json"
