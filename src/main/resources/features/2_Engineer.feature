Feature: [2] Engineer

#    TODO:
#    1. Create new Mustache template in resources/templates
#    2. Design test step to send out POST request using already created template
#    3. Fill in test data and run Scenario 1

    Scenario: [1] Successfully post a complete engineer data set
        When user sends out following engineers data:
            | name    | Lukasz    |
            | surname | Balcerzak |
            | project | NRP       |
            | role    | QA        |
        Then response is according to "schemas/engineer.json"

#    TODO:
#    1. Modify already created Mustache template in resources/templates
#    2. Fill in test data and run Scenario 2 and then Scenario 1

    Scenario: [2] Successfully post an engineer data without an assignment
        When user sends out following engineers data:
            | name    | Lukasz    |
            | surname | Balcerzak |
        Then response is according to "schemas/engineer.json"
